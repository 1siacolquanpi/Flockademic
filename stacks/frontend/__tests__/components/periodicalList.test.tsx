import { PeriodicalList } from '../../src/components/periodicalList/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should not render anything when no periodicals were passed', () => {
  const quip = shallow(<PeriodicalList periodicals={[]}/>);

  expect(quip).toBeEmptyRender();
});

it('should match the snapshot', () => {
  const periodicals = [
    { identifier: 'first', name: 'Science' },
    { identifier: 'second', name: 'Notes on Social Agents', headline: 'Representing Humans In Society' },
    { identifier: 'third', name: 'Sample journal of X' },
    // tslint:disable-next-line:max-line-length
    { identifier: 'fourth', name: 'BioBuilder Magazine', description: 'We celebrate the biodesign ideas, specifications and experimental data gathered by our BioBuilder community during workshops, through BioBuilderClubs throughout the country, and in classrooms around the world' },
    {
      description: 'Studies meant to create a carbon-free living environment.',
      headline: 'Heat your home, not the planet',
      identifier: 'fifth',
      name: 'Zero Carbon Housing',
    },
    { identifier: 'arbitrary_uuid' },
  ];
  const quip = shallow(<PeriodicalList periodicals={periodicals}/>);

  expect(toJson(quip)).toMatchSnapshot();
});
