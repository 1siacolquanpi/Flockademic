import './styles.scss';

import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { Periodical } from '../../../../../lib/interfaces/Periodical';
import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { Session } from '../../../../../lib/interfaces/Session';
import { getProfiles, getSession } from '../../services/account';
import { getPeriodical, getPeriodicalContents } from '../../services/periodical';
import { JournalOverview } from '../journalOverview/component';
import { Spinner } from '../spinner/component';

interface JournalOverviewPageState {
  articles?: null | Array<Partial<ScholarlyArticle>>;
  periodical?: null | Partial<Periodical>;
  session?: Session;
}
export interface JournalOverviewPageDispatchProps {}
export interface JournalOverviewPageOwnProps {}
export interface JournalOverviewPageRouteParams { slug: string; }

export class JournalOverviewPage
  extends React.Component<
  RouteComponentProps<JournalOverviewPageRouteParams>,
  JournalOverviewPageState
> {
  public state: JournalOverviewPageState = {};

  constructor(props: any) {
    super(props);
  }

  public async componentDidMount() {
    try {
      const periodical = await getPeriodical(this.props.match.params.slug);
      this.setState({
        periodical,
      });

      try {
        if (periodical.creator) {
          const creator = periodical.creator;
          const profiles = await getProfiles([ creator.identifier ]);

          this.setState((prevState) => ({
            periodical: {
              ...prevState.periodical,
              creator: profiles[0],
            },
          }));
        }
      } catch (e) {
        // Do nothing - we simply can't display profile info.
      }
    } catch (e) {
      this.setState({ periodical: null });
    }

    try {
      const response = await getPeriodicalContents(this.props.match.params.slug);
      this.setState({
        articles: response.hasPart,
      });
    } catch (e) {
      this.setState({ articles: null });
    }

    try {
      const session = await getSession();
      this.setState({ session });
    } catch (e) {
      // Do nothing - we simply can't check whether the current user owns the current journal.
    }
  }

  public render() {
    if (typeof this.state.periodical === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.periodical === null) {
      return (<div className="callout alert">This Journal could not be found.</div>);
    }

    return (
      <JournalOverview
        articles={this.state.articles}
        periodical={this.state.periodical}
        url={this.props.match.url}
        session={this.state.session}
      />
    );
  }
}
